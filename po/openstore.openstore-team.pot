# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the openstore.openstore-team package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: openstore.openstore-team\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-09-28 02:35+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: ../qml/AppDetailsPage.qml:109 ../qml/AppLocalDetailsPage.qml:34
msgid "App details"
msgstr ""

#: ../qml/AppDetailsPage.qml:116 ../qml/AppDetailsPage.qml:385
#: ../qml/AppLocalDetailsPage.qml:41 ../qml/Dialogs/UninstallDialog.qml:33
msgid "Remove"
msgstr ""

#: ../qml/AppDetailsPage.qml:152 ../qml/Components/PackageListItem.qml:34
#: ../qml/Components/PackageTile.qml:79
msgid "App"
msgstr ""

#: ../qml/AppDetailsPage.qml:153 ../qml/Components/HookIcon.qml:67
#: ../qml/Components/PackageListItem.qml:35
#: ../qml/Components/PackageTile.qml:80
msgid "Scope"
msgstr ""

#: ../qml/AppDetailsPage.qml:154 ../qml/Components/PackageListItem.qml:36
#: ../qml/Components/PackageTile.qml:81
msgid "Web App"
msgstr ""

#: ../qml/AppDetailsPage.qml:155 ../qml/Components/PackageListItem.qml:37
#: ../qml/Components/PackageTile.qml:82
msgid "Web App+"
msgstr ""

#: ../qml/AppDetailsPage.qml:239
msgid ""
"The installed version of this app doesn't come from the OpenStore server. "
"You can install the latest stable update by tapping the button below."
msgstr ""

#: ../qml/AppDetailsPage.qml:264 ../qml/AppLocalDetailsPage.qml:103
#: ../qml/Components/HighlightedApp.qml:110
msgid "Open"
msgstr ""

#: ../qml/AppDetailsPage.qml:280
msgid "The OpenStore is installed!"
msgstr ""

#: ../qml/AppDetailsPage.qml:289
msgid "Install stable version"
msgstr ""

#: ../qml/AppDetailsPage.qml:292
msgid "Upgrade"
msgstr ""

#: ../qml/AppDetailsPage.qml:295 ../qml/Dialogs/InstallWarningDialog.qml:41
msgid "Install"
msgstr ""

#: ../qml/AppDetailsPage.qml:371
msgid "This app is not compatible with your system."
msgstr ""

#: ../qml/AppDetailsPage.qml:404
msgid ""
"This app has access to restricted parts of the system and all of your data, "
"see below for details."
msgstr ""

#: ../qml/AppDetailsPage.qml:407
msgid "This app has access to restricted system data, see below for details."
msgstr ""

#: ../qml/AppDetailsPage.qml:432
msgid "Description"
msgstr ""

#. TRANSLATORS: Title of the changelog section
#: ../qml/AppDetailsPage.qml:470
msgid "What's New"
msgstr ""

#: ../qml/AppDetailsPage.qml:489
msgid "Packager/Publisher"
msgstr ""

#: ../qml/AppDetailsPage.qml:498 ../qml/AppLocalDetailsPage.qml:137
msgid "Installed version"
msgstr ""

#: ../qml/AppDetailsPage.qml:508
msgid "Latest available version"
msgstr ""

#: ../qml/AppDetailsPage.qml:517
msgid "First released"
msgstr ""

#: ../qml/AppDetailsPage.qml:528
msgid "Downloads of the latest version"
msgstr ""

#: ../qml/AppDetailsPage.qml:537
msgid "Total downloads"
msgstr ""

#: ../qml/AppDetailsPage.qml:546
msgid "License"
msgstr ""

#: ../qml/AppDetailsPage.qml:547
msgid "N/A"
msgstr ""

#: ../qml/AppDetailsPage.qml:557 ../qml/SettingsPage.qml:214
msgid "Source Code"
msgstr ""

#: ../qml/AppDetailsPage.qml:570
msgid "Get support for this app"
msgstr ""

#: ../qml/AppDetailsPage.qml:583
msgid "Donate to support this app"
msgstr ""

#. TRANSLATORS: This is the button that shows a list of all the packages from the same author. %1 is the name of the author.
#: ../qml/AppDetailsPage.qml:599
msgid "More from %1"
msgstr ""

#. TRANSLATORS: This is the button that shows a list of all the other packages in the same category. %1 is the name of the category.
#: ../qml/AppDetailsPage.qml:614
msgid "Other apps in %1"
msgstr ""

#: ../qml/AppDetailsPage.qml:620
msgid "Package contents"
msgstr ""

#: ../qml/AppDetailsPage.qml:713
msgid "Permissions"
msgstr ""

#: ../qml/AppDetailsPage.qml:718
msgid "Accounts"
msgstr ""

#: ../qml/AppDetailsPage.qml:719
msgid "Audio"
msgstr ""

#: ../qml/AppDetailsPage.qml:720
msgid "Bluetooth"
msgstr ""

#: ../qml/AppDetailsPage.qml:721
msgid "Calendar"
msgstr ""

#: ../qml/AppDetailsPage.qml:722
msgid "Camera"
msgstr ""

#: ../qml/AppDetailsPage.qml:723
msgid "Connectivity"
msgstr ""

#: ../qml/AppDetailsPage.qml:724
msgid "Contacts"
msgstr ""

#: ../qml/AppDetailsPage.qml:725
msgid "Content Exchange Source"
msgstr ""

#: ../qml/AppDetailsPage.qml:726
msgid "Content Exchange"
msgstr ""

#: ../qml/AppDetailsPage.qml:727
msgid "Debug"
msgstr ""

#: ../qml/AppDetailsPage.qml:728
msgid "History"
msgstr ""

#: ../qml/AppDetailsPage.qml:729
msgid "In App Purchases"
msgstr ""

#: ../qml/AppDetailsPage.qml:730
msgid "Keep Display On"
msgstr ""

#: ../qml/AppDetailsPage.qml:731
msgid "Location"
msgstr ""

#: ../qml/AppDetailsPage.qml:732
msgid "Microphone"
msgstr ""

#: ../qml/AppDetailsPage.qml:733
msgid "Read Music Files"
msgstr ""

#: ../qml/AppDetailsPage.qml:734
msgid "Music Files"
msgstr ""

#: ../qml/AppDetailsPage.qml:735
msgid "Networking"
msgstr ""

#: ../qml/AppDetailsPage.qml:736
msgid "Read Picture Files"
msgstr ""

#: ../qml/AppDetailsPage.qml:737
msgid "Picture Files"
msgstr ""

#: ../qml/AppDetailsPage.qml:738
msgid "Push Notifications"
msgstr ""

#: ../qml/AppDetailsPage.qml:739
msgid "Sensors"
msgstr ""

#: ../qml/AppDetailsPage.qml:740
msgid "User Metrics"
msgstr ""

#: ../qml/AppDetailsPage.qml:741
msgid "Read Video Files"
msgstr ""

#: ../qml/AppDetailsPage.qml:742
msgid "Video Files"
msgstr ""

#: ../qml/AppDetailsPage.qml:743
msgid "Video"
msgstr ""

#: ../qml/AppDetailsPage.qml:744
msgid "Webview"
msgstr ""

#: ../qml/AppDetailsPage.qml:748
msgid "Full System Access"
msgstr ""

#. TRANSLATORS: this will show when an app doesn't need any special permissions
#: ../qml/AppDetailsPage.qml:753
msgid "none required"
msgstr ""

#: ../qml/AppDetailsPage.qml:789
msgid "Read paths"
msgstr ""

#: ../qml/AppDetailsPage.qml:806
msgid "Write paths"
msgstr ""

#. TRANSLATORS: %1 is the size of a file, expressed in GB
#: ../qml/AppDetailsPage.qml:877
msgid "%1 GB"
msgstr ""

#. TRANSLATORS: %1 is the size of a file, expressed in MB
#: ../qml/AppDetailsPage.qml:882
msgid "%1 MB"
msgstr ""

#. TRANSLATORS: %1 is the size of a file, expressed in kB
#: ../qml/AppDetailsPage.qml:887
msgid "%1 kB"
msgstr ""

#. TRANSLATORS: %1 is the size of a file, expressed in bytes
#: ../qml/AppDetailsPage.qml:890
msgid "%1 bytes"
msgstr ""

#: ../qml/AppLocalDetailsPage.qml:117
msgid "This app could not be found in the OpenStore"
msgstr ""

#: ../qml/AppLocalDetailsPage.qml:120
msgid ""
"You are currently offline and the app details could not be fetched from the "
"OpenStore"
msgstr ""

#: ../qml/CategoriesPage.qml:39
msgid "Categories"
msgstr ""

#: ../qml/Components/BottomEdgePageStack.qml:101
msgid "Back"
msgstr ""

#: ../qml/Components/BottomEdgePageStack.qml:101
#: ../qml/Components/ReviewPreview.qml:103
#: ../qml/Components/ReviewPreview.qml:117
msgid "Close"
msgstr ""

#: ../qml/Components/HeaderMain.qml:27 ../qml/SettingsPage.qml:29
msgid "Settings"
msgstr ""

#: ../qml/Components/HeaderMain.qml:40 ../qml/SearchPage.qml:35
msgid "Search"
msgstr ""

#: ../qml/Components/HighlightedApp.qml:107
#: ../qml/Components/ReviewPreview.qml:193
msgid "Update"
msgstr ""

#: ../qml/Components/HighlightedApp.qml:113
msgid "Details"
msgstr ""

#: ../qml/Components/HookIcon.qml:54
msgid "Application"
msgstr ""

#. TRANSLATORS: This is an Ubuntu platform service for launching other applications (ref. https://developer.ubuntu.com/en/phone/platform/guides/url-dispatcher-guide/ )
#: ../qml/Components/HookIcon.qml:81
msgid "URL Handler"
msgstr ""

#. TRANSLATORS: This is an Ubuntu platform service for content exchange (ref. https://developer.ubuntu.com/en/phone/platform/guides/content-hub-guide/ )
#: ../qml/Components/HookIcon.qml:95
msgid "Content Hub Handler"
msgstr ""

#. TRANSLATORS: This is an Ubuntu platform service for push notifications (ref. https://developer.ubuntu.com/en/phone/platform/guides/push-notifications-client-guide/ )
#: ../qml/Components/HookIcon.qml:109
msgid "Push Helper"
msgstr ""

#. TRANSLATORS: i.e. Online Accounts (ref. https://developer.ubuntu.com/en/phone/platform/guides/online-accounts-developer-guide/ )
#: ../qml/Components/HookIcon.qml:123
msgid "Accounts provider"
msgstr ""

#: ../qml/Components/PackageTile.qml:98
msgid "Update available"
msgstr ""

#: ../qml/Components/PackageTile.qml:98
msgid "✓ Installed"
msgstr ""

#: ../qml/Components/ReviewPreview.qml:93
msgid "Something went wrong..."
msgstr ""

#: ../qml/Components/ReviewPreview.qml:99
msgid "Error Posting Review"
msgstr ""

#: ../qml/Components/ReviewPreview.qml:113
msgid "Review Posted Correctly"
msgstr ""

#: ../qml/Components/ReviewPreview.qml:114
msgid "Your review has been posted successfully."
msgstr ""

#: ../qml/Components/ReviewPreview.qml:128
msgid "Loading..."
msgstr ""

#: ../qml/Components/ReviewPreview.qml:128
msgid "Rate this app"
msgstr ""

#: ../qml/Components/ReviewPreview.qml:135
msgid "(Optional) Write a review"
msgstr ""

#: ../qml/Components/ReviewPreview.qml:139
msgid "%1/%2 characters"
msgstr ""

#: ../qml/Components/ReviewPreview.qml:193
msgid "Submit"
msgstr ""

#: ../qml/Components/ReviewPreview.qml:220 ../qml/Dialogs/FilterDialog.qml:143
#: ../qml/Dialogs/InstallWarningDialog.qml:49
#: ../qml/Dialogs/UnconfinedWarningDialog.qml:41
#: ../qml/Dialogs/UninstallDialog.qml:42
msgid "Cancel"
msgstr ""

#: ../qml/Components/ReviewPreview.qml:244
msgid "%1 review. "
msgid_plural "%1 reviews. "
msgstr[0] ""
msgstr[1] ""

#: ../qml/Components/ReviewPreview.qml:252
msgid "Edit Review"
msgstr ""

#: ../qml/Components/ReviewPreview.qml:255
msgid "Review app"
msgstr ""

#: ../qml/Components/ReviewPreview.qml:258
msgid "Sign in to review this app"
msgstr ""

#: ../qml/Components/ReviewPreview.qml:261
msgid "Install this app to review it"
msgstr ""

#: ../qml/Dialogs/DomainWarningDialog.qml:26
#: ../qml/Dialogs/UnconfinedWarningDialog.qml:25
msgid "Warning"
msgstr ""

#: ../qml/Dialogs/DomainWarningDialog.qml:35
msgid ""
"You are currently using a non-standard domain for the OpenStore. This is a "
"development feature. The domain you are using is:"
msgstr ""

#: ../qml/Dialogs/DomainWarningDialog.qml:49
msgid "Are you sure you want to continue?"
msgstr ""

#: ../qml/Dialogs/DomainWarningDialog.qml:53
msgid "Yes, I know what I'm doing"
msgstr ""

#: ../qml/Dialogs/DomainWarningDialog.qml:61
msgid "Get me out of here!"
msgstr ""

#: ../qml/Dialogs/DonationDialog.qml:25
msgid "Donating"
msgstr ""

#: ../qml/Dialogs/DonationDialog.qml:26
msgid "Would you like to support this app with a donation to the developer?"
msgstr ""

#: ../qml/Dialogs/DonationDialog.qml:32
msgid "Donate now"
msgstr ""

#: ../qml/Dialogs/DonationDialog.qml:40
msgid "Maybe later"
msgstr ""

#: ../qml/Dialogs/FilterDialog.qml:26
msgid "Filters and Sorting"
msgstr ""

#: ../qml/Dialogs/FilterDialog.qml:38
msgid "Sort By"
msgstr ""

#: ../qml/Dialogs/FilterDialog.qml:45 ../qml/Dialogs/FilterDialog.qml:65
msgid "Relevance"
msgstr ""

#: ../qml/Dialogs/FilterDialog.qml:46
msgid "Most Popular"
msgstr ""

#: ../qml/Dialogs/FilterDialog.qml:47
msgid "Least Popular"
msgstr ""

#: ../qml/Dialogs/FilterDialog.qml:48
msgid "Title (A-Z)"
msgstr ""

#: ../qml/Dialogs/FilterDialog.qml:49
msgid "Title (Z-A)"
msgstr ""

#: ../qml/Dialogs/FilterDialog.qml:50
msgid "Newest"
msgstr ""

#: ../qml/Dialogs/FilterDialog.qml:51
msgid "Oldest"
msgstr ""

#: ../qml/Dialogs/FilterDialog.qml:52
msgid "Latest Update"
msgstr ""

#: ../qml/Dialogs/FilterDialog.qml:53
msgid "Oldest Update"
msgstr ""

#: ../qml/Dialogs/FilterDialog.qml:91
msgid "Type"
msgstr ""

#: ../qml/Dialogs/FilterDialog.qml:98 ../qml/Dialogs/FilterDialog.qml:112
msgid "All Types"
msgstr ""

#: ../qml/Dialogs/FilterDialog.qml:99
msgid "Apps"
msgstr ""

#: ../qml/Dialogs/FilterDialog.qml:100
msgid "Web Apps"
msgstr ""

#: ../qml/Dialogs/FilterDialog.qml:135
msgid "Apply"
msgstr ""

#: ../qml/Dialogs/InstallWarningDialog.qml:27
msgid "Install unknown app?"
msgstr ""

#: ../qml/Dialogs/InstallWarningDialog.qml:28
msgid "Do you want to install the unknown app %1?"
msgstr ""

#: ../qml/Dialogs/NegativeReviewDialog.qml:26
msgid "Review confirmation"
msgstr ""

#: ../qml/Dialogs/NegativeReviewDialog.qml:27
msgid ""
"Would you like to contact the author or file a bug instead? By opening a "
"discussion with the developer(s) you could be helping to improve this app."
msgstr ""

#: ../qml/Dialogs/NegativeReviewDialog.qml:37
msgid "Get support"
msgstr ""

#: ../qml/Dialogs/NegativeReviewDialog.qml:47
msgid "Post my review"
msgstr ""

#: ../qml/Dialogs/UnconfinedWarningDialog.qml:26
msgid ""
"This app has access to restricted parts of the system and all of your data. "
"It has the potential break your system. While the OpenStore maintainers have "
"reviewed the code for this app for safety, they are not responsible for "
"anything bad that might happen to your device or data from installing this "
"app."
msgstr ""

#: ../qml/Dialogs/UnconfinedWarningDialog.qml:32
msgid "I understand the risks"
msgstr ""

#: ../qml/Dialogs/UninstallDialog.qml:25
msgid "Remove package"
msgstr ""

#: ../qml/Dialogs/UninstallDialog.qml:26
msgid "Do you want to remove %1?"
msgstr ""

#: ../qml/DiscoverPage.qml:40
msgid "OpenStore"
msgstr ""

#: ../qml/DiscoverPage.qml:73
msgid "by %1"
msgstr ""

#: ../qml/DiscoverPage.qml:112
msgid "You are currently offline"
msgstr ""

#: ../qml/DiscoverPage.qml:128
msgid "OpenStore update available"
msgstr ""

#: ../qml/DiscoverPage.qml:158
msgid " (%1 update available)"
msgid_plural " (%1 updates available)"
msgstr[0] ""
msgstr[1] ""

#: ../qml/DiscoverPage.qml:158 ../qml/DiscoverPage.qml:159
#: ../qml/InstalledAppsPage.qml:52
msgid "Installed Apps"
msgstr ""

#: ../qml/DiscoverPage.qml:183
msgid "Browse Apps by Category"
msgstr ""

#: ../qml/FilteredAppList.qml:109
msgid "No results found."
msgstr ""

#: ../qml/FilteredAppList.qml:109
msgid "Nothing here yet"
msgstr ""

#: ../qml/FilteredAppList.qml:110
msgid "No app has been released in this category yet."
msgstr ""

#: ../qml/FilteredAppList.qml:110
msgid "Try with a different search."
msgstr ""

#. TRANSLATORS: %1 is the number of installed apps
#: ../qml/InstalledAppsPage.qml:80
msgid "Installed apps (%1)"
msgstr ""

#. TRANSLATORS: %1 is the number of available app updates
#: ../qml/InstalledAppsPage.qml:84
msgid "Available updates (%1)"
msgstr ""

#. TRANSLATORS: %1 is the number of apps that can be downgraded
#: ../qml/InstalledAppsPage.qml:88
msgid "Stable version available (%1)"
msgstr ""

#: ../qml/InstalledAppsPage.qml:91
msgid ""
"The installed versions of these apps did not come from the OpenStore but a "
"stable version is available."
msgstr ""

#: ../qml/InstalledAppsPage.qml:92
msgid "Update all"
msgstr ""

#: ../qml/InstalledAppsPage.qml:194
msgid "No apps found"
msgstr ""

#: ../qml/InstalledAppsPage.qml:195
msgid "No app has been installed from OpenStore yet."
msgstr ""

#: ../qml/Main.qml:260 ../qml/SearchPage.qml:55
msgid "Filters"
msgstr ""

#: ../qml/Main.qml:307
msgid "App installed"
msgstr ""

#: ../qml/Main.qml:308
msgid "The app has been installed successfully."
msgstr ""

#: ../qml/Main.qml:311 ../qml/Main.qml:324 ../qml/Main.qml:339
msgid "OK"
msgstr ""

#: ../qml/Main.qml:320
msgid "Installation failed"
msgstr ""

#: ../qml/Main.qml:321
msgid ""
"The package could not be installed. Make sure it is a valid click package."
msgstr ""

#: ../qml/Main.qml:335
msgid "Installation failed (Error %1)"
msgstr ""

#: ../qml/SearchPage.qml:48
msgid "Search in OpenStore..."
msgstr ""

#: ../qml/SettingsPage.qml:113
msgid "OpenStore Account"
msgstr ""

#: ../qml/SettingsPage.qml:121 ../qml/SignInPage.qml:31
msgid "Sign in"
msgstr ""

#: ../qml/SettingsPage.qml:138
msgid "Sign out"
msgstr ""

#: ../qml/SettingsPage.qml:166
msgid "Parental control"
msgstr ""

#: ../qml/SettingsPage.qml:172
msgid "Hide adult-oriented content"
msgstr ""

#: ../qml/SettingsPage.qml:182
msgid ""
"By typing your password you take full responsibility for showing NSFW "
"content."
msgstr ""

#: ../qml/SettingsPage.qml:207
msgid "About OpenStore"
msgstr ""

#: ../qml/SettingsPage.qml:223
msgid "Report an issue"
msgstr ""

#: ../qml/SettingsPage.qml:232
msgid "Additional Icons by"
msgstr ""

#: openstore.desktop.in.h:1
msgid ""
"software;apps;applications;programs;store;market;play;click;discover;install;"
msgstr ""
