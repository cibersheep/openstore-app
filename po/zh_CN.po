msgid ""
msgstr ""
"Project-Id-Version: Chinese (Simplified) (OpenStore)\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-31 04:27+0000\n"
"PO-Revision-Date: 2021-04-17 18:25+0000\n"
"Last-Translator: Enceka <enceka@yeah.net>\n"
"Language-Team: Chinese (Simplified) <https://translate.ubports.com/projects/"
"openstore/openstore-app/zh_Hans/>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 3.11.3\n"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:114
#: /home/brian/dev/openstore/openstore-app/openstore/AppLocalDetailsPage.qml:33
msgid "App details"
msgstr "应用程序的详细信息"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:121
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:386
#: /home/brian/dev/openstore/openstore-app/openstore/AppLocalDetailsPage.qml:40
#: /home/brian/dev/openstore/openstore-app/openstore/Components/UninstallPopup.qml:33
msgid "Remove"
msgstr "删除"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:157
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageListItem.qml:17
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageTile.qml:62
msgid "App"
msgstr "应用"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:158
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:50
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageListItem.qml:18
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageTile.qml:63
msgid "Scope"
msgstr "Scope"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:159
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageListItem.qml:19
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageTile.qml:64
msgid "Web App"
msgstr "网页应用"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:160
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageListItem.qml:20
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageTile.qml:65
msgid "Web App+"
msgstr "网页应用+"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:240
msgid ""
"The installed version of this app doesn't come from the OpenStore server. "
"You can install the latest stable update by tapping the button below."
msgstr ""
"这个安装版本的应用不是来自 OpenStore 服务器。您可以通过点击下面的按钮来安装最"
"新的稳定更新。"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:265
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HighlightedApp.qml:109
msgid "Open"
msgstr "打开应用"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:281
msgid "The OpenStore is installed!"
msgstr "OpenStore已安装！"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:290
msgid "Install stable version"
msgstr "安装稳定版"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:293
msgid "Upgrade"
msgstr "升级"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:296
#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:358
msgid "Install"
msgstr "安装"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:372
msgid "This app is not compatible with your system."
msgstr "此应用与你的系统不兼容。"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:405
msgid ""
"This app has access to restricted parts of the system and all of your data, "
"see below for details."
msgstr "这个应用程序有权访问受限的系统数据，详情见下文。"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:408
msgid "This app has access to restricted system data, see below for details."
msgstr "这个应用程序有权访问受限的系统数据，详情见下文。"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:433
msgid "Description"
msgstr "描述"

#. TRANSLATORS: Title of the changelog section
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:466
msgid "What's New"
msgstr "最新特性"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:485
msgid "Packager/Publisher"
msgstr "打包者/发布者"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:486
msgid "OpenStore team"
msgstr "OpenStore 团队"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:494
#: /home/brian/dev/openstore/openstore-app/openstore/AppLocalDetailsPage.qml:115
msgid "Installed version"
msgstr "已安装的版本"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:504
msgid "Latest available version"
msgstr "最新的可用版本"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:513
msgid "First released"
msgstr "首次发布"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:524
msgid "Downloads of the latest version"
msgstr "下载最新版本"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:533
msgid "Total downloads"
msgstr "总计下载"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:542
msgid "License"
msgstr "许可证"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:543
msgid "N/A"
msgstr "N/A"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:553
#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:215
msgid "Source Code"
msgstr "源代码"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:566
msgid "Get support for this app"
msgstr "获得对此应用的支持"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:579
msgid "Donate to support this app"
msgstr "捐赠以支持此应用"

#. TRANSLATORS: This is the button that shows a list of all the packages from the same author. %1 is the name of the author.
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:595
#, qt-format
msgid "More from %1"
msgstr "%1 的更多应用"

#. TRANSLATORS: This is the button that shows a list of all the other packages in the same category. %1 is the name of the category.
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:610
#, qt-format
msgid "Other apps in %1"
msgstr "%1 的其他应用"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:616
msgid "Package contents"
msgstr "程序包的内容"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:709
msgid "Permissions"
msgstr "权限"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:714
msgid "Accounts"
msgstr "帐号提供商"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:715
msgid "Audio"
msgstr "声音"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:716
msgid "Bluetooth"
msgstr "蓝牙"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:717
msgid "Calendar"
msgstr "日历"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:718
msgid "Camera"
msgstr "相机"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:719
msgid "Connectivity"
msgstr "连接"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:720
msgid "Contacts"
msgstr "联系人"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:721
msgid "Content Exchange Source"
msgstr "内容交换源"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:722
msgid "Content Exchange"
msgstr "内容交换"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:723
msgid "Debug"
msgstr "调试"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:724
msgid "History"
msgstr "历史"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:725
msgid "In App Purchases"
msgstr "应用内购买"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:726
msgid "Keep Display On"
msgstr "保持亮屏"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:727
msgid "Location"
msgstr "位置"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:728
msgid "Microphone"
msgstr "麦克风"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:729
msgid "Read Music Files"
msgstr "读取音乐文件"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:730
msgid "Music Files"
msgstr "音乐文件"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:731
msgid "Networking"
msgstr "网络"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:732
msgid "Read Picture Files"
msgstr "读取图像文件"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:733
msgid "Picture Files"
msgstr "图像文件"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:734
msgid "Push Notifications"
msgstr "推送通知"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:735
msgid "Sensors"
msgstr "传感器"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:736
msgid "User Metrics"
msgstr "用户指标"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:737
msgid "Read Video Files"
msgstr "读取视频文件"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:738
msgid "Video Files"
msgstr "视频文件"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:739
msgid "Video"
msgstr "视频"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:740
msgid "Webview"
msgstr "网页视图"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:744
msgid "Full System Access"
msgstr "完全系统访问"

#. TRANSLATORS: this will show when an app doesn't need any special permissions
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:749
msgid "none required"
msgstr "无权限要求"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:785
msgid "Read paths"
msgstr "读取路径"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:802
msgid "Write paths"
msgstr "写入路径"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:817
msgid "Donating"
msgstr "捐赠"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:818
msgid "Would you like to support this app with a donation to the developer?"
msgstr "你想通过捐赠开发者来支持此应用吗？"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:824
msgid "Donate now"
msgstr "现在捐赠"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:832
msgid "Maybe later"
msgstr "以后再说"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:845
#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:297
msgid "Warning"
msgstr "警告"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:846
msgid ""
"This app has access to restricted parts of the system and all of your data. "
"It has the potential break your system. While the OpenStore maintainers have "
"reviewed the code for this app for safety, they are not responsible for "
"anything bad that might happen to your device or data from installing this "
"app."
msgstr ""
"这个应用有权限访问系统限制区和你的所有数据，这存在潜在破坏系统的风险，但是OpenStore的维护者们已经审阅过此应用代码的安全性，他们不承担安装此应用对"
"你的设备和数据可能造成的影响。"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:852
msgid "I understand the risks"
msgstr "我明白此风险"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:861
#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:366
#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:82
#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:198
#: /home/brian/dev/openstore/openstore-app/openstore/Components/UninstallPopup.qml:42
msgid "Cancel"
msgstr "取消"

#. TRANSLATORS: %1 is the size of a file, expressed in GB
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:880
#, qt-format
msgid "%1 GB"
msgstr "%1 GB"

#. TRANSLATORS: %1 is the size of a file, expressed in MB
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:885
#, qt-format
msgid "%1 MB"
msgstr "%1 MB"

#. TRANSLATORS: %1 is the size of a file, expressed in kB
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:890
#, qt-format
msgid "%1 kB"
msgstr "%1 kB"

#. TRANSLATORS: %1 is the size of a file, expressed in bytes
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:893
#, qt-format
msgid "%1 bytes"
msgstr "%1 字节"

#: /home/brian/dev/openstore/openstore-app/openstore/AppLocalDetailsPage.qml:95
msgid "This app could not be found in the OpenStore"
msgstr "无法在OpenStore找到此应用"

#: /home/brian/dev/openstore/openstore-app/openstore/AppLocalDetailsPage.qml:98
msgid ""
"You are currently offline and the app details could not be fetched from the "
"OpenStore"
msgstr "你当前处于离线无法从OpenStore获取应用的详细信息"

#: /home/brian/dev/openstore/openstore-app/openstore/CategoriesTab.qml:38
msgid "Categories"
msgstr "分类"

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:65
#, qt-format
msgid "by %1"
msgstr "通过 %1"

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:98
msgid "OpenStore update available"
msgstr "OpenStore 可用更新"

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:134
#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:135
#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:51
msgid "Installed Apps"
msgstr "已安装应用"

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:134
#, qt-format
msgid " (%1 update available)"
msgid_plural " (%1 updates available)"
msgstr[0] " （%1个可用的更新）"

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:159
msgid "Browse Apps by Category"
msgstr "分类浏览应用"

#: /home/brian/dev/openstore/openstore-app/openstore/FilteredAppView.qml:106
msgid "Nothing here yet"
msgstr "这里还没有什么"

#: /home/brian/dev/openstore/openstore-app/openstore/FilteredAppView.qml:106
msgid "No results found."
msgstr "未找到任何结果。"

#: /home/brian/dev/openstore/openstore-app/openstore/FilteredAppView.qml:107
msgid "No app has been released in this category yet."
msgstr "此类别中尚未发布任何应用程序。"

#: /home/brian/dev/openstore/openstore-app/openstore/FilteredAppView.qml:107
msgid "Try with a different search."
msgstr "尝试使用不同的搜索。"

#. TRANSLATORS: %1 is the number of installed apps
#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:79
#, qt-format
msgid "Installed apps (%1)"
msgstr "已安装应用(%1)"

#. TRANSLATORS: %1 is the number of available app updates
#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:83
#, qt-format
msgid "Available updates (%1)"
msgstr "可用的更新(%1)"

#. TRANSLATORS: %1 is the number of apps that can be downgraded
#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:87
#, qt-format
msgid "Stable version available (%1)"
msgstr "可用的稳定版  (%1)"

#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:90
msgid ""
"The installed versions of these apps did not come from the OpenStore but a "
"stable version is available."
msgstr "已安装的应用不是来自 OpenStore 但有稳定版本可供下载。"

#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:91
msgid "Update all"
msgstr "全部更新"

#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:193
msgid "No apps found"
msgstr "未找到应用"

#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:194
msgid "No app has been installed from OpenStore yet."
msgstr "还没有从 OpenStore 安装任何应用。"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:306
msgid ""
"You are currently using a non-standard domain for the OpenStore. This is a "
"development feature. The domain you are using is:"
msgstr "您目前正在为 OpenStore 使用非标准网域，这是一个开发特质。 您正在使用的网域是："

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:320
msgid "Are you sure you want to continue?"
msgstr "你确定要继续吗？"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:324
msgid "Yes, I know what I'm doing"
msgstr "是的，我知道我在做什么"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:332
msgid "Get me out of here!"
msgstr "带我离开！"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:344
msgid "Install unknown app?"
msgstr "安装未知的应用程序吗？"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:345
#, qt-format
msgid "Do you want to install the unknown app %1?"
msgstr "您想安装 %1 吗？"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:386
msgid "App installed"
msgstr "应用程序已安装"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:387
msgid "The app has been installed successfully."
msgstr "应用程序已成功安装。"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:390
#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:403
#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:418
msgid "OK"
msgstr "确认"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:399
msgid "Installation failed"
msgstr "安装失败"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:400
msgid ""
"The package could not be installed. Make sure it is a valid click package."
msgstr "无法安装该软件包。请确保它是一个有效点击软件包。"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:414
#, qt-format
msgid "Installation failed (Error %1)"
msgstr "安装失败(错误 %1)"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:29
msgid "Your passphrase is required to access restricted content"
msgstr "访问受限内容需要密码"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:30
msgid "Your passcode is required to access restricted content"
msgstr "访问受限内容需要密码"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:39
msgid "Authentication failed"
msgstr "验证失败"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:47
msgid "Passphrase required"
msgstr "需要密码"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:47
msgid "Passcode required"
msgstr "需要密码"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:59
msgid "passphrase (default is 0000)"
msgstr "密码（默认 0000）"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:59
msgid "passcode (default is 0000)"
msgstr "密码（默认 0000）"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:71
msgid "Authentication failed. Please retry"
msgstr "认证失败，请重试"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:76
msgid "Authenticate"
msgstr "身份验证"

#: /home/brian/dev/openstore/openstore-app/openstore/SearchTab.qml:16
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HeaderMain.qml:39
msgid "Search"
msgstr "搜索"

#: /home/brian/dev/openstore/openstore-app/openstore/SearchTab.qml:29
msgid "Search in OpenStore..."
msgstr "在 OpenStore 中的搜索..."

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:27
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HeaderMain.qml:26
msgid "Settings"
msgstr "设置"

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:114
msgid "OpenStore Account"
msgstr "OpenStore 账号"

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:122
#: /home/brian/dev/openstore/openstore-app/openstore/SignInWebView.qml:30
msgid "Sign in"
msgstr "登陆"

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:139
msgid "Sign out"
msgstr "注销"

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:167
msgid "Parental control"
msgstr "家长控制"

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:173
msgid "Hide adult-oriented content"
msgstr "隐藏成人内容"

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:183
msgid ""
"By typing your password you take full responsibility for showing NSFW "
"content."
msgstr "通过输入密码，您将对显示的 NSFW 内容承担全部的责任。"

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:208
msgid "About OpenStore"
msgstr "关于 OpenStore"

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:224
msgid "Report an issue"
msgstr "报告问题"

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:233
msgid "Additional Icons by"
msgstr "额外图标"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/BottomEdgePageStack.qml:84
msgid "Back"
msgstr "返回"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/BottomEdgePageStack.qml:84
#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:83
#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:97
msgid "Close"
msgstr "关闭"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/HighlightedApp.qml:106
#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:187
msgid "Update"
msgstr "更新"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/HighlightedApp.qml:112
msgid "Details"
msgstr "详细信息"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:37
msgid "Application"
msgstr "应用程序"

#. TRANSLATORS: This is an Ubuntu platform service for launching other applications (ref. https://developer.ubuntu.com/en/phone/platform/guides/url-dispatcher-guide/ )
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:64
msgid "URL Handler"
msgstr "URL 处理程序"

#. TRANSLATORS: This is an Ubuntu platform service for content exchange (ref. https://developer.ubuntu.com/en/phone/platform/guides/content-hub-guide/ )
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:78
msgid "Content Hub Handler"
msgstr "内容处理程序"

#. TRANSLATORS: This is an Ubuntu platform service for push notifications (ref. https://developer.ubuntu.com/en/phone/platform/guides/push-notifications-client-guide/ )
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:92
msgid "Push Helper"
msgstr "推送助手"

#. TRANSLATORS: i.e. Online Accounts (ref. https://developer.ubuntu.com/en/phone/platform/guides/online-accounts-developer-guide/ )
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:106
msgid "Accounts provider"
msgstr "帐号提供商"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageTile.qml:81
msgid "Update available"
msgstr "可用的更新"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageTile.qml:81
msgid "✓ Installed"
msgstr "✓ 安装"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:73
msgid "Something went wrong..."
msgstr "出现一些问题..."

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:79
msgid "Error Posting Review"
msgstr "错误报告备注"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:93
msgid "Review Posted Correctly"
msgstr "评论提交正确"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:94
msgid "Your review has been posted successfully."
msgstr "你的评论成功提交。"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:108
msgid "Rate this app"
msgstr "支持此应用"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:108
msgid "Loading..."
msgstr "载入..."

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:129
msgid "(Optional) Write a review"
msgstr "(可选）添加一个评论"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:133
#, qt-format
msgid "%1/%2 characters"
msgstr "%1到%2个字符"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:222
#, qt-format
msgid "%1 review. "
msgid_plural "%1 reviews. "
msgstr[0] "%1 评论 "

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:228
msgid "Sign in to review this app"
msgstr "登陆评论此应用"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:229
msgid "Review app"
msgstr "评价此应用"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:230
msgid "Install this app to review it"
msgstr "安装这个应用来评价它"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/TextualButtonStyle.qml:46
#: /home/brian/dev/openstore/openstore-app/openstore/Components/TextualButtonStyle.qml:54
#: /home/brian/dev/openstore/openstore-app/openstore/Components/TextualButtonStyle.qml:60
msgid "Pick"
msgstr "选择"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/UninstallPopup.qml:25
msgid "Remove package"
msgstr "删除程序包"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/UninstallPopup.qml:26
#, qt-format
msgid "Do you want to remove %1?"
msgstr "你想要删除 %1 吗？"

#~ msgid "Discover"
#~ msgstr "发现"

#~ msgid "Update OpenStore now!"
#~ msgstr "现在更新 OpenStore！"

#~ msgid "My Apps"
#~ msgstr "我的应用程序"

#~ msgid "AppArmor profile"
#~ msgstr "AppArmor 配置文件"

#~ msgid ""
#~ "OpenStore allows installing unconfined applications. Please make sure "
#~ "that you know about the implications of that."
#~ msgstr "OpenStore 允许安装不受限制的应用程序。请确保您所知道的涉及问题。"

#~ msgid ""
#~ "An unconfined application has the ability to break the system, reduce its "
#~ "performance and/or spy on you."
#~ msgstr "不受限制的应用程序有能力破坏系统，降低其性能和/或监视您。"

#~ msgid ""
#~ "While we are doing our best to prevent that by reviewing applications, we "
#~ "don't take any responsibility if something bad slips through."
#~ msgstr ""
#~ "虽然我们正在以防止，通过查看应用程序，我们不承担任何责任如果东西坏溜了进"
#~ "来。虽然我们正在以防止，通过查看应用程序，我们不承担任何责任如果东西坏溜了"
#~ "进来。"

#~ msgid "Use this at your own risk."
#~ msgstr "使用此您自担风险。"

#~ msgid "Okay. Got it! I'll be careful."
#~ msgstr "好吧。明白了！我会小心。"

#~ msgid "Developers"
#~ msgstr "开发者"

#~ msgid "Manage your apps on OpenStore"
#~ msgstr "在 OpenStore 上管理应用程序"

#~ msgid "Fetching package list..."
#~ msgstr "正在获取软件包列表..."
